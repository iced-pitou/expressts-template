import { Request, Response } from 'express';

export { getHelloWorld };

function getHelloWorld(req: Request, res: Response) {
    res.status(200).json({ success: true, data: 'Hello World' });
}
