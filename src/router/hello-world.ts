import * as express from 'express';
import { getHelloWorld } from '../controller/hello-world';

export const helloWorldRouter = express.Router();
helloWorldRouter.use(express.json());

helloWorldRouter
    .route('/')
    .get(getHelloWorld);