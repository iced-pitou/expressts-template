import express from 'express';
import route from './config/route.json';
import { helloWorldRouter } from './router/hello-world';
import pgClient from './util/pg-client';

// Configuration
const app = express();
app.use(express.static('src/public'));
app.use(route.helloWorld, helloWorldRouter);

// Starting
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`App listening on port ${port}...`);
    pgClient.connect();
});

// Shutdown event-handling
for (let signal of ['SIGINT', 'SIGTERM']) {
    process.on(signal, () => {
        console.log('Application shutdown');
        pgClient.end();
        process.exit();
    });
}